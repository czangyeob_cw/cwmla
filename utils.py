import datetime
import hashlib
import torch
import fasttext
from pycountry import languages

import numpy as np
import cv2
import albumentations
from albumentations.pytorch import ToTensorV2
import torch.nn.functional as F
from PIL import Image
import logging
import os

#  cnn model 불러오기
model_ft = torch.load('cnn_project_prediction_model.pth')
model_ft.eval()
# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
device = "cpu"

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler = logging.FileHandler('query.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


def defaultconverter(o):
  if isinstance(o, datetime.datetime):
      return o.__str__()

def calc_file_hash(path):
    f = open(path, 'rb')
    data = f.read() 
    hash = hashlib.md5(data).hexdigest()
    return hash

def get_thumbnail(image_path):
    save_to = image_path.replace("/data/", "/thumbnail/")
    print(image_path, save_to)
    print("/".join(save_to.split('/')[:-1]))
    os.makedirs("/".join(save_to.split('/')[:-1]), exist_ok=True)
    #save_to = '.'.join(image_path.split('.')[:-1]) + '.thumbnail.' + image_path.split('.')[-1]
    try:
        im = Image.open(image_path)
        im.thumbnail((180,180))
        im = im.convert("RGB")
        im.save(save_to)
        im.close()
        print(f" - Success: {save_to}")
        return save_to
    except:
        os.system(f'cp {image_path} {save_to}')
        print(f" - Fail: {save_to} is not thumbnail")
        return False

def margin_confidence(prob_dist, sorted=False):
    if not sorted:
        prob_dist[::-1].sort() # sort probs so that largest is at prob_dist[0]
    difference = (prob_dist[0] - prob_dist[1])
    margin_conf = 1 - difference 
    return margin_conf

def entropy_score(prob_dist):
    log_probs = prob_dist * np.log2(prob_dist) # multiply each probability by its base 2 log
    raw_entropy = 0-np.sum(log_probs)
    normalized_entropy = raw_entropy / np.log2(prob_dist.size)
    return normalized_entropy


def uncertainty_score(image_path):
    idx_to_class={0: 'ocr', 1: 'detection', 2: 'segmentation', 3: 'face', 4: 'pose'}
    class_to_idx={'ocr': 0, 'detection': 1, 'segmentation': 2, 'face': 3, 'pose': 4}
    test_transforms = albumentations.Compose(
        [albumentations.Resize(224, 224),
         albumentations.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
         ToTensorV2(),])
    
    # label=image_path.split('/')[6]
    image=cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    image_tensor = test_transforms(image=image)['image'].float()
    image_tensor = image_tensor.unsqueeze_(0)
    input = image_tensor
    input = input.to(device)
    output = model_ft(input)
    
    prob = F.softmax(output, dim=1)
    top_p, top_class = prob.topk(5, dim = 1)
    top_class=top_class.detach().cpu().numpy()[0]
    top_class=[idx_to_class[i] for i in top_class]
    top_p=top_p.detach().cpu().numpy()[0]
    top_class_and_prob={i:v for i,v in zip(top_class,top_p)}
    
    probability_score=prob.detach().cpu().numpy()[0]
    margin_score=margin_confidence(probability_score)
    entropy=entropy_score(probability_score)

    index = output.data.cpu().numpy().argmax()
    prediction=idx_to_class[index]
    
    row={'image_path':image_path, 'prediction': prediction, 'top_class_k': top_class_and_prob, 
         'margin_score': margin_score, "entropy_score":entropy}
    
    return row

def cnn_prediction(image):
    idx_to_class={0: 'ocr', 1: 'detection', 2: 'segmentation', 3: 'face', 4: 'pose'}
    test_transforms = albumentations.Compose(
    [albumentations.Resize(224, 224),
     albumentations.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
     ToTensorV2(),])
         
    image_tensor = test_transforms(image=image)['image'].float()
    image_tensor = image_tensor.unsqueeze_(0)
    input = image_tensor
    input = input.to(device)
    output = model_ft(input)
    index = output.data.cpu().numpy().argmax()
    
    prediction=idx_to_class[index]
    return prediction


PRETRAINED_LANGUAGE_MODEL_PATH = './engine/fasttext/lid.176.bin'
model_lang = fasttext.load_model(PRETRAINED_LANGUAGE_MODEL_PATH)

def predict_language(sentece):
    lang = model_lang.predict(sentece)[0][0].split('__')[-1]
    if len(lang)==3:
        lang_name = languages.get(alpha_3=lang).name
    else:
        lang_name = languages.get(alpha_2=lang).name
    return lang_name
