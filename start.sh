export FLASK_APP=app.py
export FLASK_RUN_HOST=0.0.0.0
export FLASK_RUN_PORT=8080
flask run --cert='./cert/ml.crowdworks.kr/star_crowdworks_kr_cert.crt' --key='./cert/ml.crowdworks.kr/key_nopass.pem'
