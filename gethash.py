import hashlib

def calc_file_hash(path):
    f = open(path, 'rb')
    data = f.read()
    hash = hashlib.md5(data).hexdigest()
    return hash

print(calc_file_hash("./data/0f1041e29a2a08be9c066b4e328eb442/000756287.jpg"))
