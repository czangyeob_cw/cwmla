import os
import sys
import glob
from PIL import Image


def get_thumbnail(image_path):
    save_to = image_path.replace("/data/", "/thumbnail/")
    print("thumbnail_info1: ", image_path, save_to)
    print("thumbnail_info2: ","/".join(save_to.split('/')[:-1]))
    os.makedirs("/".join(save_to.split('/')[:-1]), exist_ok=True)
    #save_to = '.'.join(image_path.split('.')[:-1]) + '.thumbnail.' + image_path.split('.')[-1]
    if True:#try:
        im = Image.open(image_path)
        im.thumbnail((180,180))
        im = im.convert("RGB")
        im.save(save_to)
        im.close()
        print(f" - Success: {save_to}")
        return save_to
    else: #except:
        print(f" - Fail: {save_to}")
        return False



for path in glob.glob(os.path.join("./data/test/",'**/*'), recursive=True): #("./data/f553d96da04c0f5fe0238fdc036e44ab/"):
    if os.path.isfile(path):    get_thumbnail(path)
