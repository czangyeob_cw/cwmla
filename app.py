# -*- encoding: utf-8 -*-
from importlib.metadata import files
from flask import Blueprint, Flask, render_template, request, send_file, Response, send_from_directory
from flask_jsonpify import jsonify
from flask_cors import CORS
import json
import os
import zipfile
import pymysql
import datetime
import glob
from utils import *
from celery import Task
import time
import cv2
from collections import Counter, defaultdict
import ssl

from celery import Celery

def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


app = Flask(__name__)
CORS(app)


app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379/1',
    CELERY_TASK_TRACK_STARTED = True,
    # CELERY_ACCEPT_CONTENT = ['json'],
    # CELERY_TASK_SERIALIZER = 'json',
    # CELERY_RESULT_SERIALIZER = 'json',
    CELERYD_LOG_FILE = os.path.join('./', 'celery.logs'),  
    CELERYD_LOG_LEVEL = "INFO",
    CELERY_ACKS_LATE = True # 작업이 랜덤하게 빠지는 현상으로 추가
)
celery = make_celery(app)

def getconn():
    conn = pymysql.Connect(host='localhost', user='root', password='mysql.1234', database='cwmla')
    #conn = pymysql.Connect(host='localhost', user='root', password='mysql.1234', database='cwmla_dev')
    #conn = pymysql.Connect(host='34.64.236.46', port=3306, user='tester', password='pw123', database='cwmla_dev')  #GCP SQL 인스턴스 - test DB
    return conn

def db_execute(query):
    logger.info(query)
    conn = getconn()
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
    except:
        logger.info(f"wrong query: {query}")
        result = (())
    conn.commit()
    conn.close()
    return result

class CallbackTask(Task):
    # 비동기 작업 성공시
    def on_success(self, retval, task_id, args, kwargs):
        query = "UPDATE transaction_log SET status='done', update_timestamp='{}' WHERE task_id='{}'".format(datetime.datetime.now().__str__().split('.')[0], task_id) # 
        db_execute(query)
        
        result = celery.AsyncResult(task_id).result

        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM transaction_log WHERE task_id = '{}';".format(task_id)
        transaction_id = db_execute(query)[0][0]

        # apply_async에서 리턴해준 결과를 활용해 결과 구성
        result_count = Counter(result['result'])
        print(1)
        result_count['total'] = len(result['result']) # 전체 갯수
        total_keys = ['ocr', 'segmentation', 'detection', 'pose', 'face', 'none']
        for k in total_keys:
            if k not in list(result_count.keys()): # Counter 결과 중에 5개 분류 또는 미처리(none)이 없는 경우 만들어준다.
                result_count[k] = 0

        ocr_files, bbox_files, seg_files, facelandmark_files, pose_files, none_files = [], [], [], [], [], []

        print(2)
        for filename, domain in zip(result['filename'], result['result']):
            # filename = './' + '/'.join(filename.split('/')[3:]) # data, md5hash로 만든 디렉터리 제거
            if domain == 'ocr':
                ocr_files.append(filename)
            elif domain == 'detection':
                bbox_files.append(filename)
            elif domain == 'segmentation':
                seg_files.append(filename)
            elif domain == 'face':
                facelandmark_files.append(filename)
            elif domain == 'pose':
                pose_files.append(filename)
            else:
                none_files.append(filename)

        print(3, seg_files)
        filenames_json = {
            "ocr": ocr_files,
            "bbox": bbox_files,
            "seg": seg_files,
            "facelandmark": facelandmark_files,
            "pose": pose_files,
            "none": none_files
        }

        print(4, filenames_json)
        thumbnails_json = {
            "ocr": [tj.replace("/data/","/thumbnail/") for tj in ocr_files], 
            "bbox": [tj.replace("/data/","/thumbnail/") for tj in bbox_files],
            "seg": [tj.replace("/data/","/thumbnail/") for tj in seg_files],
            "facelandmark": [tj.replace("/data/","/thumbnail/") for tj in facelandmark_files], 
            "pose": [tj.replace("/data/","/thumbnail/") for tj in pose_files], 
            "none": [tj.replace("/data/","/thumbnail/") for tj in none_files]
        }
            
        print(5, thumbnails_json)
        # 분류 결과를 classification_result 테이블에 저장한다.
        #query = """ INSERT INTO classification_result (transaction_id, number_of_data, number_of_ocr, number_of_bbox, number_of_seg, number_of_facelandmark, number_of_pose, number_of_none, filenames) VALUES ({}, {}, {}, {}, {}, {}, {}, {}, \'{}\');""".format(transaction_id, len(result['result']), result_count['ocr'], result_count['detection'], result_count['segmentation'], result_count['face'], result_count['pose'], result_count['none'], json.dumps(filenames_json))
        query = " ".join([
            "INSERT INTO classification_result ",
            "(transaction_id, number_of_data, number_of_ocr, number_of_bbox, number_of_seg, number_of_facelandmark, number_of_pose, number_of_none, filenames, thumbnails)",
            f"VALUE ({transaction_id}, {len(result['result'])}, {result_count['ocr']}, {result_count['detection']}, {result_count['segmentation']}, {result_count['face']}, {result_count['pose']}, {result_count['none']}, '{json.dumps(filenames_json)}', '{json.dumps(thumbnails_json)}');"        
            ])

        db_execute(query)

    # 비동기 작업 실패시
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        # 실패한 경우 트랜젝션 로그에서 상태를 'failed' 라고 표기한다.
        conn = getconn()
        cursor = conn.cursor()
        query = "UPDATE transaction_log SET status='failed', update_timestamp='{}' WHERE task_id='{}'".format(datetime.datetime.now().__str__().split('.')[0], task_id) # 
        cursor.execute(query)
        conn.commit()
        # 실패한 경우 분류 결과 표를 0으로 초기화한다.
        ## task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM transaction_log WHERE task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        ## 분류 결과를 classification_result 테이블에 저장한다.
        query = """ INSERT INTO classification_result (transaction_id, number_of_data, number_of_ocr, number_of_bbox, number_of_seg, number_of_facelandmark, number_of_pose, number_of_none) 
                    VALUES ({}, {}, {}, {}, {}, {}, {}, {});""".format(transaction_id, 0, 0, 0, 0, 0, 0, 0)
        cursor.execute(query)
        conn.commit()
        conn.close()


@celery.task(bind=True, base=CallbackTask)
def cwmla(self, file_list):
    result_list = []
    filename_list = []
    uncertainty_list = []
    for idx, filename in enumerate(file_list):
        # try:
        if os.path.isfile(filename):
            print(filename)
            image = cv2.imread(filename, cv2.IMREAD_COLOR)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

            # prediction = cnn_prediction(image)
            # {'image_path':image_path, 'prediction': prediction, 'top_class_k': top_class_and_prob, 'margin_score': margin_score, "entropy_score":entropy}
            score = uncertainty_score(filename)
            prediction = score['prediction']
            self.update_state(state='PROGRESS',
                                meta={
                                    'current': idx,
                                    'total': len(file_list) - 1,
                                    'filename': filename,
                                    'prediction': prediction})

            # Uncertainty 기준 미처리
            if score['entropy_score'] > 0.592:
                result_list.append('none')
            else:
                result_list.append(prediction)
            filename_list.append(filename)
            uncertainty_list.append(score['entropy_score'])
            print("Uncertainty score" +  str(score['entropy_score']) +  "Filename : " + filename)
            # print(score)
        # except:
        #     continue
    return {'status': 'job finished.', 'result': result_list, 'filename': filename_list}


class OcrPredictionCallbackTask(Task):
    # 비동기 작업 성공시
    def on_success(self, retval, task_id, args, kwargs):
        result = celery.AsyncResult(task_id).result
        summary = result['summary']

        res_json  = []
        type_list = summary.keys()
        for lang in type_list:
            res_json.append({"type": lang, "count": summary[lang]})

        conn = getconn()
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE ocr_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()
        type_file_map = result['type_file_map']
        _ = cwmla_push_task_summary(transaction_id, 'ocr', result=res_json, files=type_file_map, status_code=1)

        # 캐시 저장
        
        
    # 비동기 작업 실패시
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        conn = getconn()    
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE ocr_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()

        # 실패한 경우 빈 객체 입력하고 종료로 표시
        _ = cwmla_push_task_summary(transaction_id, 'ocr', result=[], files={}, status_code=2)


@celery.task(bind=True, base=OcrPredictionCallbackTask)
def cwmla_ocr(self, file_list):
    import easyocr
    from collections import Counter
    transaction_id = file_list[-1]
    file_list = file_list[:-1]

    lang_list = []
    type_file_map = {}
    
    reader = easyocr.Reader(['ko','en'], gpu=False) # this needs to run only once to load the model into memory
    
    hash_list = []
    for filename in file_list :
        hash_list.append(calc_file_hash(filename))
    preprocessed = {}
    query = f"SELECT file_hash, ocr_summary, entropy_score FROM previous_result WHERE file_hash in ({json.dumps(hash_list)[1:-1]});"
    result = db_execute(query)
    for file_hash, ocr_summary, entropy_score in result:
        preprocessed[file_hash] = {}
        preprocessed[file_hash]["ocr_summary"] = ocr_summary
        preprocessed[file_hash]["entropy_score"] = entropy_score

    for idx, image_name in enumerate(file_list):
        # try:
        
        file_hash = calc_file_hash(image_name)
        if file_hash not in list(preprocessed.keys()):
            result = reader.readtext(image_name)
            string = ""
            for item in result:
                string += item[1]
                string += " "
            print(string)
            lang = predict_language(string)
        else:
            print("OCR CACHE")
            try:
                lang = list(json.loads(preprocessed[file_hash]["ocr_summary"]).keys())[0]
            except:
                lang = "None"

        lang_list.append(lang)

        if lang in list(type_file_map.keys()):
            type_file_map[lang].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
        else:
            type_file_map[lang] = []
            type_file_map[lang].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
        #except:
        #    print("FAILED :", image_name)

    summary_json = Counter(lang_list)
    #caching
    #summary_json = defaultdict(int)
    #for k, v in Counter(lang_list).items():
    #    summary_json[k] = v
    #prev_rows = db_execute(f"SELECT id, ocr_summary FROM task_summary WHERE transaction_id = {transaction_id};")
    #for prev_row in prev_rows:
    #    prev_id, prev_summary = prev_row
    #    for _obj in json.loads(prev_summary):
    #        summary_json[_obj['type']] += _obj['count']
    #print(summary_json)
    return {'status': 'job finished.', 'summary': dict(summary_json), 'type_file_map': type_file_map}


class BboxPredictionCallbackTask(Task):
    # 비동기 작업 성공시
    def on_success(self, retval, task_id, args, kwargs):
        result = celery.AsyncResult(task_id).result
        summary = result['summary']

        res_json  = []
        type_list = summary.keys()
        for obj_class in type_list:
            res_json.append({"type": obj_class, "count": summary[obj_class]})

        conn = getconn()
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE bbox_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()
        type_file_map = result['type_file_map']
        _ = cwmla_push_task_summary(transaction_id, 'bbox', result=res_json, files=type_file_map, status_code=1)

    # 비동기 작업 실패시
    def on_failure(self, exc, task_id, args, kwargs, einfo):        
        conn = getconn()
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE bbox_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()
        # 실패한 경우 빈 객체 입력하고 종료로 표시
        _ = cwmla_push_task_summary(transaction_id, 'bbox', result=[], status_code=2)

@celery.task(bind=True, base=BboxPredictionCallbackTask)
def cwmla_bbox(self, file_list):
    from mmdet.apis import inference_detector, init_detector
    from collections import Counter
    transaction_id = file_list[-1]
    file_list = file_list[:-1]

    summary_json = Counter()
    det_config = './engine/mmdetection/configs/faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py'
    det_checkpoint = './engine/mmdetection/checkpoints/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth'
    model = init_detector(det_config, det_checkpoint, device='cpu')

    classes = {
        1: u'person',
        2: u'bicycle',
        3: u'car',
        4: u'motorcycle',
        5: u'airplane',
        6: u'bus',
        7: u'train',
        8: u'truck',
        9: u'boat',
        10: u'traffic light',
        11: u'fire hydrant',
        12: u'stop sign',
        13: u'parking meter',
        14: u'bench',
        15: u'bird',
        16: u'cat',
        17: u'dog',
        18: u'horse',
        19: u'sheep',
        20: u'cow',
        21: u'elephant',
        22: u'bear',
        23: u'zebra',
        24: u'giraffe',
        25: u'backpack',
        26: u'umbrella',
        27: u'handbag',
        28: u'tie',
        29: u'suitcase',
        30: u'frisbee',
        31: u'skis',
        32: u'snowboard',
        33: u'sports ball',
        34: u'kite',
        35: u'baseball bat',
        36: u'baseball glove',
        37: u'skateboard',
        38: u'surfboard',
        39: u'tennis racket',
        40: u'bottle',
        41: u'wine glass',
        42: u'cup',
        43: u'fork',
        44: u'knife',
        45: u'spoon',
        46: u'bowl',
        47: u'banana',
        48: u'apple',
        49: u'sandwich',
        50: u'orange',
        51: u'broccoli',
        52: u'carrot',
        53: u'hot dog',
        54: u'pizza',
        55: u'donut',
        56: u'cake',
        57: u'chair',
        58: u'couch',
        59: u'potted plant',
        60: u'bed',
        61: u'dining table',
        62: u'toilet',
        63: u'tv',
        64: u'laptop',
        65: u'mouse',
        66: u'remote',
        67: u'keyboard',
        68: u'cell phone',
        69: u'microwave',
        70: u'oven',
        71: u'toaster',
        72: u'sink',
        73: u'refrigerator',
        74: u'book',
        75: u'clock',
        76: u'vase',
        77: u'scissors',
        78: u'teddy bear',
        79: u'hair drier',
        80: u'toothbrush'
    }
    type_file_map = {}
    
    hash_list = []
    for filename in file_list :
        hash_list.append(calc_file_hash(filename))
    preprocessed = {}
    query = f"SELECT file_hash, bbox_summary, entropy_score FROM previous_result WHERE file_hash in ({json.dumps(hash_list)[1:-1]});"
    result = db_execute(query)
    for file_hash, bbox_summary, entropy_score in result:
        preprocessed[file_hash] = {}
        preprocessed[file_hash]["bbox_summary"] = bbox_summary
        preprocessed[file_hash]["entropy_score"] = entropy_score
        ####################

    for idx, image_name in enumerate(file_list):
        try:
            file_hash = calc_file_hash(image_name)
            if file_hash not in list(preprocessed.keys()): # 캐쉬에 없는 경우

                result_json = {}
                result = inference_detector(model, image_name)
                
                for class_idx, data in enumerate(result):
                    class_name = classes[class_idx + 1]
                    result_json[class_name] = 0 # 해당 객체 개수 기록

                    if data.shape[0] != 0:
                        # print(class_name)
                        for obj in data:
                            if obj[4] >= 0.4: # filter by confidence score
                                result_json[class_name] += 1
                    
                    if result_json[class_name] == 0:
                        del result_json[class_name] # confidence score가 0.4 이상이 되는 경우가 없는 경우 키 삭제
                    else:
                        if class_name in list(type_file_map.keys()):
                            type_file_map[class_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
                        else:
                            type_file_map[class_name] = []
                            type_file_map[class_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
            else: # 캐시에 있는 경우
                print("BBOX CACHE")
                try:
                    result_json = json.loads(preprocessed[file_hash]["bbox_summary"])
                except:
                    result_json = {}

                for class_name in result_json.keys():
                    if class_name in list(type_file_map.keys()):
                        type_file_map[class_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
                    else:
                        type_file_map[class_name] = []
                        type_file_map[class_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})

            print(result_json)
            # {'person': 3, 'car': 46, 'truck': 3, 'bench': 2, 'suitcase': 1, 'chair': 2}
            summary_json += Counter(result_json)

        except:
            print("FAILED :", image_name)

    return {'status': 'job finished.', 'summary': dict(summary_json), 'type_file_map': type_file_map}


class SegPredictionCallbackTask(Task):
    # 비동기 작업 성공시
    def on_success(self, retval, task_id, args, kwargs):
        result = celery.AsyncResult(task_id).result
        summary = result['summary']

        res_json  = []
        type_list = summary.keys()
        for obj_class in type_list:
            res_json.append({"type": obj_class, "count": summary[obj_class]})

        conn = getconn()
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE seg_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()
        type_file_map = result['type_file_map']
        _ = cwmla_push_task_summary(transaction_id, 'seg', result=res_json, files=type_file_map, status_code=1)

    # 비동기 작업 실패시
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        conn = getconn()        
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE seg_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()
        # 실패한 경우 빈 객체 입력하고 종료로 표시
        _ = cwmla_push_task_summary(transaction_id, 'seg', result=[], files={}, status_code=2)

@celery.task(bind=True, base=SegPredictionCallbackTask)
def cwmla_seg(self, file_list):
    from mmseg.apis import inference_segmentor, init_segmentor
    from collections import Counter
    import cv2
    import numpy as np
    transaction_id = file_list[-1]
    file_list = file_list[:-1]

    # CPU 로 돌리기 위해서 SyncBN --> BN 으로 수정
    # 컨피그 파일을 수정하는 데, 만약 해당 컨피그 파일이 Base 에 다른 모델(백본)을 포함하고 있다면, 해당 파일에서도 SyncBN을 BN으로 바꿔줘야 한다.
    config_file = './engine/mmsegmentation/configs/ocrnet/ocrnet_hr48_512x512_160k_ade20k.py'
    checkpoint_file = './engine/mmsegmentation/checkpoints/ocrnet_hr48_512x512_160k_ade20k_20200615_184705-a073726d.pth'
    model = init_segmentor(config_file, checkpoint_file, device='cpu')
    type_file_map = {}

    summary_json = Counter()
    classes=['wall', 'building', 'sky', 'floor', 'tree', 'ceiling', 'road', 'bed ',
    'windowpane', 'grass', 'cabinet', 'sidewalk', 'person', 'earth',
    'door', 'table', 'mountain', 'plant', 'curtain', 'chair', 'car',
    'water', 'painting', 'sofa', 'shelf', 'house', 'sea', 'mirror', 'rug',
    'field', 'armchair', 'seat', 'fence', 'desk', 'rock', 'wardrobe',
    'lamp', 'bathtub', 'railing', 'cushion', 'base', 'box', 'column',
    'signboard', 'chest of drawers', 'counter', 'sand', 'sink',
    'skyscraper', 'fireplace', 'refrigerator', 'grandstand', 'path',
    'stairs', 'runway', 'case', 'pool table', 'pillow', 'screen door',
    'stairway', 'river', 'bridge', 'bookcase', 'blind', 'coffee table',
    'toilet', 'flower', 'book', 'hill', 'bench', 'countertop', 'stove',
    'palm', 'kitchen island', 'computer', 'swivel chair', 'boat', 'bar',
    'arcade machine', 'hovel', 'bus', 'towel', 'light', 'truck', 'tower',
    'chandelier', 'awning', 'streetlight', 'booth', 'television receiver',
    'airplane', 'dirt track', 'apparel', 'pole', 'land', 'bannister',
    'escalator', 'ottoman', 'bottle', 'buffet', 'poster', 'stage', 'van',
    'ship', 'fountain', 'conveyer belt', 'canopy', 'washer', 'plaything',
    'swimming pool', 'stool', 'barrel', 'basket', 'waterfall', 'tent',
    'bag', 'minibike', 'cradle', 'oven', 'ball', 'food', 'step', 'tank',
    'trade name', 'microwave', 'pot', 'animal', 'bicycle', 'lake',
    'dishwasher', 'screen', 'blanket', 'sculpture', 'hood', 'sconce',
    'vase', 'traffic light', 'tray', 'ashcan', 'fan', 'pier', 'crt screen',
    'plate', 'monitor', 'bulletin board', 'shower', 'radiator', 'glass',
    'clock', 'flag']

    preprocessed = {}
    hash_list = []
    for filename in file_list :
        hash_list.append(calc_file_hash(filename))
        
    query = f"SELECT file_hash, seg_summary, entropy_score FROM previous_result WHERE file_hash in ({json.dumps(hash_list)[1:-1]});"
    result = db_execute(query)
    for file_hash, seg_summary, entropy_score in result:
        preprocessed[file_hash] = {}
        preprocessed[file_hash]["seg_summary"] = seg_summary
        preprocessed[file_hash]["entropy_score"] = entropy_score


    for idx, image_name in enumerate(file_list):
        try:
            file_hash = calc_file_hash(image_name)
            if file_hash not in list(preprocessed.keys()): # 캐쉬에 없는 경우

                res_json = {}
                result = inference_segmentor(model, image_name)
                result = result[0]
                unique, counts = np.unique(result, return_counts=True)

                for object_num, count in zip(list(unique), list(counts)):
                    res_json[classes[object_num]] = int(count) # 각 객체 별로 몇 픽셀씩 차지하고 있는지 계산
                    
                    if classes[object_num] in list(type_file_map.keys()):
                        type_file_map[classes[object_num]].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
                    else:
                        type_file_map[classes[object_num]] = []
                        type_file_map[classes[object_num]].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})

                print(res_json)
                summary_json += Counter(res_json)
            else:
                print("SEG CACHE")
                try:
                    res_json = json.loads(preprocessed[file_hash]['seg_summary'])
                except:
                    res_json = {}
                summary_json += Counter(res_json)

                for class_name in res_json.keys():
                    if class_name in list(type_file_map.keys()):
                        type_file_map[class_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
                    else:
                        type_file_map[class_name] = []
                        type_file_map[class_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
     
        except:
            print("FAILED :", image_name)

    #caching
    #ssummary_json = defaultdict(int)
    ##for k, v in summary_json.items():
    #    ssummary_json[k] = v
    #prev_rows = db_execute(f"SELECT id, ocr_summary FROM task_summary WHERE transaction_id = {transaction_id};")
    #for prev_row in prev_rows:
    #    prev_id, prev_summary = prev_row
    #    for _obj in json.loads(prev_summary):
    #        ssummary_json[_obj['type']] += _obj['count']
    #print(ssummary_json) 
    return {'status': 'job finished.', 'summary': dict(summary_json), 'type_file_map': type_file_map}


class FacePredictionCallbackTask(Task):
    # 비동기 작업 성공시
    def on_success(self, retval, task_id, args, kwargs):
        result = celery.AsyncResult(task_id).result
        summary = result['summary']

        res_json  = []
        type_list = summary.keys()
        for obj_class in type_list:
            res_json.append({"type": obj_class, "count": summary[obj_class]})

        conn = getconn()
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE face_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()
        type_file_map = result['type_file_map']
        _ = cwmla_push_task_summary(transaction_id, 'face', result=res_json, files=type_file_map, status_code=1)

    # 비동기 작업 실패시
    def on_failure(self, exc, task_id, args, kwargs, einfo):    
        conn = getconn()    
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE face_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()

        # 실패한 경우 빈 객체 입력하고 종료로 표시
        _ = cwmla_push_task_summary(transaction_id, 'face', result=[], files={}, status_code=2)


@celery.task(bind=True, base=FacePredictionCallbackTask)
def cwmla_face(self, file_list):
    import cv2
    import dlib
    from collections import Counter
    transaction_id = file_list[-1]
    file_list = file_list[:-1]

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("./engine/facelandmark/shape_predictor_68_face_landmarks.dat")
    total_faces = 0
    type_file_map = {}
    tmp = []

    preprocessed = {}
    hash_list = []
    for filename in file_list :
        hash_list.append(calc_file_hash(filename))
    query = f"SELECT file_hash, face_summary, entropy_score FROM previous_result WHERE file_hash in ({json.dumps(hash_list)[1:-1]});"
    result = db_execute(query)
    for file_hash, face_summary, entropy_score in result:
        preprocessed[file_hash] = {}
        preprocessed[file_hash]["face_summary"] = face_summary
        preprocessed[file_hash]["entropy_score"] = entropy_score

    for idx, image_name in enumerate(file_list):
        try:
            file_hash = calc_file_hash(image_name)
            if file_hash not in list(preprocessed.keys()): # 캐쉬에 없는 경우
                image = cv2.imread(image_name, cv2.IMREAD_COLOR)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                rects = detector(image)
                total_faces += len(rects) # 전체 얼굴 계산 --> Summary

                if len(rects) == 0 or len(rects) == 1: # type 이름은 얼굴 몇개 인지
                    type_name = str(len(rects)) + ' face'
                else:
                    type_name = str(len(rects)) + ' faces'
                tmp.append(type_name)
                if type_name in list(type_file_map.keys()):
                    type_file_map[type_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
                else:
                    type_file_map[type_name] = []
                    type_file_map[type_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
            else: # 캐시에 있는 경우
                print("FACE CACHE")
                try:
                    res_json = json.loads(preprocessed[file_hash]['face_summary'])
                    type_name = list(res_json.keys())[0]
                except:
                    res_json = {}
                    type_name = '0 face'
                tmp.append(type_name)

                if type_name in list(type_file_map.keys()):
                    type_file_map[type_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
                else:
                    type_file_map[type_name] = []
                    type_file_map[type_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
        except:
            print("FAILED :", image_name)

    summary_json = Counter(tmp)
    #caching
    #summary_json = defaultdict(int)
    #for k, v in Counter(tmp).items():
    #    summary_json[k] = v
    #prev_rows = db_execute(f"SELECT id, ocr_summary FROM task_summary WHERE transaction_id = {transaction_id};")
    #for prev_row in prev_rows:
    #    prev_id, prev_summary = prev_row
    #    for _obj in json.loads(prev_summary):
    #        summary_json[_obj['type']] += _obj['count'] 
    #print(summary_json)
    return {'status': 'job finished.', 'summary': dict(summary_json), 'type_file_map': type_file_map}

class PosePredictionCallbackTask(Task):
    # 비동기 작업 성공시
    def on_success(self, retval, task_id, args, kwargs):
        result = celery.AsyncResult(task_id).result
        summary = result['summary']

        res_json  = []
        type_list = summary.keys()
        for obj_class in type_list:
            res_json.append({"type": obj_class, "count": summary[obj_class]})
        conn = getconn()
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE pose_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()
        type_file_map = result['type_file_map']
        _ = cwmla_push_task_summary(transaction_id, 'pose', result=res_json, files=type_file_map, status_code=1)

    # 비동기 작업 실패시
    def on_failure(self, exc, task_id, args, kwargs, einfo):        
        conn = getconn()
        cursor = conn.cursor()
        # task_id를 사용해 transaction_id 구하기
        query = "SELECT transaction_id FROM task_info WHERE pose_task_id = '{}';".format(task_id)
        cursor.execute(query)
        transaction_id = cursor.fetchall()[0][0]
        conn.commit()
        conn.close()
        # 실패한 경우 빈 객체 입력하고 종료로 표시
        _ = cwmla_push_task_summary(transaction_id, 'pose', result=[], files={}, status_code=2)


@celery.task(bind=True, base=PosePredictionCallbackTask)
def cwmla_pose(self, file_list):
    from collections import Counter
    transaction_id = file_list[-1]
    file_list = file_list[:-1]

    try:
        from mmdet.apis import inference_detector, init_detector
        has_mmdet = True
    except (ImportError, ModuleNotFoundError):
            has_mmdet = False
    from mmpose.apis import (inference_top_down_pose_model, init_pose_model,
                            process_mmdet_results, vis_pose_result)

    for_summary = []
    type_file_map = {}
    hash_list = []
    for filename in file_list :
        hash_list.append(calc_file_hash(filename))

    # initialize detection model
    det_config = './engine/mmdetection/configs/faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py'
    det_checkpoint = './engine/mmdetection/checkpoints/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth'
    det_model = init_detector(det_config, det_checkpoint, device='cpu')
    # initialize pose model
    pose_config = './engine/mmpose/configs/body/2d_kpt_sview_rgb_img/topdown_heatmap/coco/hrnet_w48_coco_256x192.py'
    pose_checkpoint = './engine/mmpose/checkpoints/hrnet_w48_coco_256x192-b9e0b3ab_20200708.pth'
    pose_model = init_pose_model(pose_config, pose_checkpoint, device='cpu')

    preprocessed = {}
    query = f"SELECT file_hash, pose_summary, entropy_score FROM previous_result WHERE file_hash in ({json.dumps(hash_list)[1:-1]});"
    result = db_execute(query)
    for file_hash, pose_summary, entropy_score in result:
        preprocessed[file_hash] = {}
        preprocessed[file_hash]["pose_summary"] = pose_summary
        preprocessed[file_hash]["entropy_score"] = entropy_score

    for idx, image_name in enumerate(file_list):
        try:
            file_hash = calc_file_hash(image_name)
            if file_hash not in list(preprocessed.keys()): # 캐쉬에 없는 경우

                # inference detection
                mmdet_results = inference_detector(det_model, image_name)

                # extract person (COCO_ID=1) bounding boxes from the detection results
                person_results = process_mmdet_results(mmdet_results, cat_id=1)

                # inference pose
                pose_results, returned_outputs = inference_top_down_pose_model(pose_model,
                                                                            image_name,
                                                                            person_results,
                                                                            bbox_thr=0.3,
                                                                            format='xyxy',
                                                                            dataset=pose_model.cfg.data.test.type)
                print(pose_results)

                if len(pose_results) == 0 or len(pose_results) == 1: # type 이름은 얼굴 몇개 인지
                    type_name = str(len(pose_results)) + ' person'
                else:
                    type_name = str(len(pose_results)) + ' persons'
                for_summary.append(type_name)
                
                if type_name in list(type_file_map.keys()):
                    type_file_map[type_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
                else:
                    type_file_map[type_name] = []
                    type_file_map[type_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
            else:
                print("POSE CACHE")
                try:
                    res_json = json.loads(preprocessed[file_hash]['pose_summary'])
                    type_name = list(res_json.keys())[0]
                except:
                    res_json = {}
                    type_name = '0 person'
                for_summary.append(type_name)

                if type_name in list(type_file_map.keys()):
                    type_file_map[type_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
                else:
                    type_file_map[type_name] = []
                    type_file_map[type_name].append({'path': image_name, 'url': 'https://ml.crowdworks.kr:8080' + image_name[1:]})
        except:
            print("FAILED")
        
    summary_json = Counter(for_summary)

    #caching
    #summary_json = defaultdict(int)
    #for k, v in Counter(for_summary).items():
    #    summary_json[k] = v
    #prev_rows = db_execute(f"SELECT id, ocr_summary FROM task_summary WHERE transaction_id = {transaction_id};")
    #for prev_row in prev_rows:
    #    prev_id, prev_summary = prev_row
    #    for _obj in json.loads(prev_summary):
    #        summary_json[_obj['type']] += _obj['count']
    #print(summary_json)
    return {'status': 'job finished.', 'summary': dict(summary_json), 'type_file_map': type_file_map}

@app.route('/cwmla/', methods=['GET'])
def cwmla_index():
    # 업로드 페이지 렌더링
    return render_template('index.html')

@app.route('/cwmla/list', methods=['GET'])
def cwmla_list():
    conn = getconn()
    cursor = conn.cursor()
    query = "select transaction_id, name, folder, status, create_timestamp from transaction_log WHERE display_yn=1 ORDER BY transaction_id DESC;" # JSON 말린 상태에서 정렬이 잘 안됨
    # query = "SELECT JSON_ARRAYAGG(JSON_OBJECT('transaction_id', transaction_id, 'name', name, 'folder', folder, 'status', status, 'create_timestamp', create_timestamp)) from transaction_log ORDER BY transaction_id DESC;"
    cursor.execute(query)

    conn.commit()
    result = cursor.fetchall()
    print(result[0])
    result_json = []
    for item in result:
        result_json.append({'transaction_id': item[0],
                            'name': item[1],
                            'folder': item[2],
                            'status': item[3],
                            'create_timestamp': item[4],
        })
    conn.close()
    return Response(json.dumps(result_json, default = defaultconverter), content_type='application/json')
    # return Response(result, content_type='application/json')
    #https://louky0714.tistory.com/132
    #https://deviscreen.tistory.com/85 # DBeaver Public key 관련 오류

# 테스트 페이지 추후 삭제
@app.route('/cwmla/list_html', methods=['GET'])
def cwmla_list_html():
    conn = getconn()
    cursor = conn.cursor()
    query = "SELECT * FROM transaction_log ORDER BY transaction_id DESC;"
    cursor.execute(query)
    result = cursor.fetchall()
    conn.commit()
    conn.close()
    # print(result)
    return render_template('list.html',transaction_list=result)


def cwmla_push_transaction_log(name, folder, task_id, md5hash, status='in_progress', create_timestamp=datetime.datetime.now()):
    conn = getconn()
    cursor = conn.cursor()
    query = """INSERT INTO transaction_log (name, folder, status, task_id, file_hash) VALUES('{}', '{}', '{}', '{}', '{}');""".format(name, folder, status, task_id, md5hash)
    cursor.execute(query)
    conn.commit()
    conn.close()
    return cursor.lastrowid # 만들어진 transaction_id


def cwmla_push_task_summary(transaction_id, engine, result=[], files={}, status_code=0):
    new_result = []
    if engine == 'initialize':
        query = """INSERT INTO task_summary (ocr_ready, ocr_summary, ocr_files, bbox_ready, bbox_summary, bbox_files, seg_ready, seg_summary, seg_files, face_ready, face_summary, face_files, pose_ready, pose_summary, pose_files, transaction_id) VALUES({}, '{}', '{}', {}, '{}', '{}', {}, '{}', '{}', {}, '{}', '{}', {}, '{}', '{}', {});""".format(status_code, result, files, status_code, result, files, status_code, result, files, status_code, result, files,status_code, result, files, transaction_id)
        db_execute(query)
    elif engine == 'ocr':
        print("--------------------")
        for _ in result:
            if not _['type'] == "None": new_result.append(_)
        query = "UPDATE task_summary SET ocr_ready={}, ocr_summary='{}', ocr_files='{}' WHERE transaction_id={}".format(status_code, json.dumps(new_result), json.dumps(files), transaction_id) # 
        db_execute(query)
    elif engine == 'bbox':
        query = "UPDATE task_summary SET bbox_ready={}, bbox_summary='{}', bbox_files='{}' WHERE transaction_id={}".format(status_code, json.dumps(result), json.dumps(files), transaction_id) # 
        db_execute(query)
    elif engine == 'seg':
        query = "UPDATE task_summary SET seg_ready={}, seg_summary='{}', seg_files='{}' WHERE transaction_id={}".format(status_code, json.dumps(result), json.dumps(files), transaction_id) # 
        db_execute(query)
    elif engine == 'face':
        for _ in result:
            if not _['type'] == "0 face": new_result.append(_)
        query = "UPDATE task_summary SET face_ready={}, face_summary='{}', face_files='{}' WHERE transaction_id={}".format(status_code, json.dumps(new_result), json.dumps(files), transaction_id) # 
        db_execute(query)
    elif engine == 'pose':
        for _ in result:
            if not _['type'] == "0 person": new_result.append(_)
        query = "UPDATE task_summary SET pose_ready={}, pose_summary='{}', pose_files='{}' WHERE transaction_id={}".format(status_code, json.dumps(new_result), json.dumps(files), transaction_id) # 
        db_execute(query)
    return 0


# def unzip(source_file, dest_path):
#     with zipfile.ZipFile(source_file, 'r') as zf:
#         zipInfo = zf.infolist()
#         for member in zipInfo:
#             try:
#                 print(member.filename.encode('cp437').decode('euc-kr', 'ignore'))
#                 member.filename = member.filename.encode('cp437').decode('euc-kr', 'ignore')
#                 zf.extract(member, dest_path)
#             except:
#                 print(source_file)
#                 raise Exception('what?!')

@app.route('/cwmla/upload', methods=['POST'])
def cwmla_upload(): 
    # 저장할 디렉터리 생성
    os.makedirs('./data', exist_ok=True)
    # 파일 객체 불러오기
    file_content = request.files['file']
    name = request.form.get('name')

    # Zip 파일인지 여부 검사
    if zipfile.is_zipfile(file_content):
        local_path = os.path.abspath(os.path.join('./data',file_content.filename))
        file_content.stream.seek(0) # 없으면 오류 발생!
        file_content.save(local_path) # 파일 저장

        md5hash = calc_file_hash(local_path) # 압축 풀 디렉토리명 구하기 md5sum으로..
        extract_folder = os.path.join('./data/', md5hash) # 폴더명 지정
        thumbnail_folder = os.path.join('./thumbnail/', md5hash)

        # 압축파일 핸들링
        query = f"SELECT transaction_id FROM transaction_log WHERE file_hash='{md5hash}';"
        result = db_execute(query)
        if True:
            upload_zip = zipfile.ZipFile(local_path,'r') # 압축 파일 핸들
            upload_zip.extractall(extract_folder) # 폴더명에 풀기
            upload_zip.close()
            # unzip(local_path, extract_folder) # 한글 지원 시도

            print(f" + create thumbnail from {thumbnail_folder}")
            thumbnail_dict = dict()
            target_list = glob.glob(os.path.join(extract_folder,'**/*'), recursive=True)
            for image in target_list: #os.listdir(extract_folder):
                if os.path.isfile(image):
                    if not get_thumbnail(image):
                        logger.info(f"thumbnail failed: {os.path.join(image)}")

            print(f" + thumnbnail is ready! {thumbnail_folder}")

        file_list = glob.glob(os.path.join(extract_folder,'**/*'), recursive=True) # **/* subfolder, only file
        print(file_list)

        # #################
        # # For serving 
        # # 파일 이름에 공백이 있는 경우 _로 변경
        # #################
        # for filename in file_list:
        #     if " " in filename:
        #         dst_filename = filename.replace(" ","_")
        #         os.rename(filename, dst_filename)
        # file_list = glob.glob(os.path.join(extract_folder,'**/*'), recursive=True) # **/* subfolder, only file 공백 제거 후 리스트

        # 작업 관련 테이블 초기화
        task = cwmla.apply_async([file_list])
        transaction_id = cwmla_push_transaction_log(name, extract_folder, task.id, md5hash)
        _ = cwmla_push_task_summary(transaction_id, 'initialize', status_code=0)

        # 데이터 분류: 
        all_file_list = {}
        for file_name in file_list:
            if os.path.isfile(file_name):
                all_file_list[calc_file_hash(file_name)] = file_name
        
        query = f"SELECT file_hash, ocr_summary, bbox_summary, seg_summary, face_summary, pose_summary, entropy_score FROM previous_result WHERE file_hash in ({json.dumps(list(all_file_list.keys()))[1:-1]});"
        old_file_info = db_execute(query)

        ## 실시간 처리
        # 새로운 데이터 비동기 작업 요청
        task_ocr = cwmla_ocr.apply_async([list(all_file_list.values())+[transaction_id]])
        task_bbox = cwmla_bbox.apply_async([list(all_file_list.values())+[transaction_id]])
        task_seg = cwmla_seg.apply_async([list(all_file_list.values())+[transaction_id]])
        task_face = cwmla_face.apply_async([list(all_file_list.values())+[transaction_id]])
        task_pose = cwmla_pose.apply_async([list(all_file_list.values())+[transaction_id]])

        query = """INSERT INTO task_info (transaction_id, ocr_task_id, bbox_task_id, seg_task_id, face_task_id, pose_task_id) VALUES({}, '{}', '{}', '{}', '{}', '{}');""".format(transaction_id, task_ocr, task_bbox, task_seg, task_face, task_pose)
        db_execute(query)

        tmp = {
                "transaction_id": transaction_id, # 새로 생성한 ID 리턴
                "task_id" : task.id
        }
        return Response(json.dumps(tmp), content_type='application/json')

    else: # 압축 파일이 아닌 경우
        tmp = {
                "transaction_id": -1
            }
    return Response(json.dumps(tmp), content_type='application/json')



@app.route('/cwmla/content/<transaction_id>', methods=['GET'])
def cwmla_content(transaction_id):
    conn = getconn()
    cursor = conn.cursor()
    # 기본 정보 가져 오기
    query = "SELECT transaction_id, name, folder, status, create_timestamp, update_timestamp, task_id FROM transaction_log WHERE transaction_id = {};".format(transaction_id)
    cursor.execute(query)
    result = cursor.fetchall()
    transaction_id, name, folder, status, create_ts, update_ts, _ = result[0]
    
    # 처리가 완료된 정보 가져 오기
    query = "SELECT transaction_id, number_of_data, number_of_ocr, number_of_bbox, number_of_seg, number_of_facelandmark, number_of_pose, number_of_none, filenames FROM classification_result WHERE transaction_id = {};".format(transaction_id)
    cursor.execute(query)
    result = cursor.fetchall()
    conn.commit()
    conn.close()
    print(result)

    if result != ():
        transaction_id, number_of_data, number_of_ocr, number_of_bbox, number_of_seg, number_of_facelandmark, number_of_pose, number_of_none, _ = result[0]

        content_data = {
            "info": {
                "transaction_id": transaction_id,
                "name": name,
                "folder": folder,
                "status": status,
                "create_timestamp": create_ts.__str__(),
                "update_timestamp": update_ts.__str__(),
                "number_of_data": number_of_data
            },
            "results": [
                {
                    "label": "OCR",
                    "engine": "ocr",
                    "count": number_of_ocr
                },
                {
                    "label": "Bounding Box",
                    "engine": "bbox",
                    "count": number_of_bbox
                },
                {
                    "label": "Segmentation",
                    "engine": "seg",
                    "count": number_of_seg
                },
                {
                    "label": "Face Landmark",
                    "engine": "facelandmark",
                    "count": number_of_facelandmark
                },
                {
                    "label": "Pose",
                    "engine": "pose",
                    "count": number_of_pose
                },
                {
                    "label" : "None",
                    "engine": "none",
                    "count": number_of_none
                }
            ]
        }
        return Response(json.dumps(content_data), content_type='application/json')
    else:
        content_data = {
            "info": {
                "transaction_id": transaction_id,
                "name": name,
                "folder": folder,
                "status": status,
                "create_timestamp": create_ts.__str__(),
                "update_timestamp": update_ts.__str__(),
                "number_of_data": -1
            },
            "results": [
                {
                    "label": "OCR",
                    "engine": "ocr",
                    "count": 0
                },
                {
                    "label": "Bounding Box",
                    "engine": "bbox",
                    "count": 0
                },
                {
                    "label": "Segmentation",
                    "engine": "seg",
                    "count": 0
                },
                {
                    "label": "Face Landmark",
                    "engine": "facelandmark",
                    "count": 0
                },
                {
                    "label": "Pose",
                    "engine": "pose",
                    "count": 0
                },
                {
                    "label": "None",
                    "engine": "none",
                    "count": 0
                }
            ]
        }
        return Response(json.dumps(content_data), content_type='application/json')

# 테스트 페이지 추후 삭제
@app.route('/cwmla/content_html/<transaction_id>', methods=['GET'])
def cwmla_content_html(transaction_id):
    conn = getconn()
    cursor = conn.cursor()
    # 기본 정보 가져 오기
    query = "SELECT transaction_id, name, folder, status, create_ts, update_ts FROM transaction_log WHERE transaction_id = {};".format(transaction_id)
    cursor.execute(query)
    result = cursor.fetchall()
    transaction_id, name, folder, status, create_ts, update_ts = result[0]
    
    # 처리가 완료된 정보 가져 오기
    query = "SELECT * FROM classification_result WHERE transaction_id = {};".format(transaction_id)
    cursor.execute(query)
    result = cursor.fetchall()
    conn.commit()
    conn.close()
    print(result)
    transaction_id, number_of_data, number_of_ocr, number_of_bbox, number_of_seg, number_of_facelandmark, number_of_pose, number_of_none, _ = result[0]

    content_data = {
        "info": {
            "transaction_id": transaction_id,
            "name": name,
            "folder": folder,
            "status": status,
            "create_timestamp": create_ts.__str__(),
            "update_timestamp": update_ts.__str__(),
            "number_of_data": number_of_data
        },
        "result": {
            "number_of_ocr": number_of_ocr,
            "number_of_bbox": number_of_bbox,
            "number_of_seg": number_of_seg,
            "number_of_facelandmark": number_of_facelandmark,
            "number_of_pose": number_of_pose,
            "number_of_none": number_of_none
        }
    }
    return render_template('content.html',content_data=content_data)



@app.route('/cwmla/content/<transaction_id>/<engine>/ready', methods=['GET'])
def cwmla_engine_ready(transaction_id, engine):
    if engine == 'ocr':
        engine_str = 'ocr_ready'
    elif engine == 'bbox':
        engine_str = 'bbox_ready'
    elif engine == 'seg':
        engine_str = 'seg_ready'
    elif engine == 'facelandmark':
        engine_str = 'face_ready'
    elif engine == 'pose':
        engine_str = 'pose_ready'
    elif engine == 'none':
        # return Response(json.dumps({'status': 'true'}), content_type='application/json') 
        engine_str = 'none_ready'
    else:
        return Response(json.dumps({'status': 'engine not found'}), content_type='application/json')


    # 기본 정보 가져오기
    query = f"SELECT ts.{engine_str}, cr.thumbnails FROM classification_result cr INNER JOIN task_summary ts ON cr.transaction_id=ts.transaction_id WHERE cr.transaction_id={transaction_id};"
    result = db_execute(query)
    
    
    ### 응답에 "thumbnail_list" : [] 추가 - DB에서 조회

    # try:
    #     thumbnail_localpath = json.loads(result[0][1])[engine]
    # except:
    #     return Response(json.dumps({'status': 'failed'}), content_type='application/json')

    if result == ():
        return Response(json.dumps({'status': 'false'}), content_type='application/json')
    else:
        thumbnail_localpath = json.loads(result[0][1])[engine]
    

    thumbnail_list = []

    for localpath in thumbnail_localpath:
        thumbnail_list.append({'path': '/'.join(localpath.split('/')[3:]), 'url': 'https://ml.crowdworks.kr:8080' + localpath[1:]})

    #content_data = {"thumbnail_list": json.loads(result[0][1])[engine]}
    content_data = {}
    if result[0][0] == 0:
        content_data['status'] = 'false'
    elif result[0][0] == 1:
        content_data['status'] = 'true'
    elif result[0][0] == 2:
        content_data['status'] = 'failed'
    else:
        content_data['status'] = 'please check'

    content_data['thumbnail_list'] = thumbnail_list
    return Response(json.dumps(content_data), content_type='application/json')
    # {'status': 'failed'} : 처리 중
    # {'status': 'true'} : 완료
    # {'status': 'failed'} : 실패

@app.route('/cwmla/content/<transaction_id>/<engine>/summary', methods=['GET'])
def cwmla_engine_summary(transaction_id, engine):
    if engine == 'ocr':
        engine_str = 'ocr_summary'
    elif engine == 'bbox':
        engine_str = 'bbox_summary'
    elif engine == 'seg':
        engine_str = 'seg_summary'
    elif engine == 'facelandmark':
        engine_str = 'face_summary'
    elif engine == 'pose':
        engine_str = 'pose_summary'
    else:
        return Response(json.dumps({'status': 'engine not found'}), content_type='application/json')

    # 기본 정보 가져 오기
    query = " ".join(["SELECT ts.transaction_id"
        ,str(", IF(cr.number_of_ocr,ts.ocr_summary,'{}') AS ocr_summary" if engine=="ocr" else "")
        ,str(", IF(cr.number_of_bbox,ts.bbox_summary,'{}') AS bbox_summary" if engine=="bbox" else "")
        ,str(", IF(cr.number_of_seg,ts.seg_summary,'{}') AS seg_summary" if engine=="seg" else "")
        ,str(", IF(cr.number_of_pose,ts.pose_summary,'{}') AS pose_summary" if engine=="pose" else "")
        ,str(", IF(cr.number_of_facelandmark,ts.face_summary,'{}') as face_summary" if engine=="facelandmark" else "")
        ," FROM task_summary ts INNER JOIN classification_result cr ON ts.transaction_id=cr.transaction_id"
        ,f" WHERE cr.transaction_id={transaction_id};"])

    try:
        _, summary = db_execute(query)[0]
    except:
        summary = "{}"

    #query = "SELECT {} FROM task_summary WHERE transaction_id = {};".format(engine_str, transaction_id)
    #result = db_execute(query)
    return Response(json.dumps(json.loads(summary)), content_type='application/json')



@app.route('/cwmla/content_html/<transaction_id>/list', methods=['POST'])
def cwmla_file_list(transaction_id):
    conn = getconn()
    cursor = conn.cursor()
    # 파일 리스트 JSON 가져 오기
    query = "SELECT filenames FROM classification_result WHERE transaction_id = {};".format(transaction_id)
    cursor.execute(query)
    result = cursor.fetchall()
    conn.commit()
    conn.close()

    checkbox_selected = request.form.getlist('domain[]')
    print(checkbox_selected)
    data = json.loads(result[0][0])
    domain_table = {
        '0': "ocr",
        '1': "bbox",
        '2': "seg",
        '3': "facelandmark",
        '4': "pose",
        '5': "none"
    }
    result_json = {}
    for checked_idx in checkbox_selected:
        result_json[domain_table[checked_idx]] = data[domain_table[checked_idx]]
    return jsonify(result_json)

@app.route('/cwmla/content/<transaction_id>/list', methods=['POST'])
def cwmla_file_filelist(transaction_id):
    conn = getconn()
    cursor = conn.cursor()
    # 파일 리스트 JSON 가져 오기
    query = "SELECT filenames FROM classification_result WHERE transaction_id = {};".format(transaction_id)
    cursor.execute(query)
    result = cursor.fetchall()
    conn.commit()
    conn.close()

    print("!!!!!!!!!!!")
    print(result)
    selected = request.get_json()
    try:
        selected_engine = selected['engine']
    except:
        selected_engine = []

    try:
        selected_types = selected['types']
    except:
        selected_types = []

    result_json = []

    for engine in selected_engine:
        if engine == 'ocr':
            label = 'OCR'
        elif engine == 'bbox':
            label = 'Bounding Box'
        elif engine == 'seg':
            label = 'Sementation'
        elif engine == 'facelandmark':
            label = 'Face Landmark'
        elif engine == 'pose':
            label = 'Pose'
        else:
            label = 'None'        

        paths = []
        file_list = json.loads(result[0][0])[engine]
        for filename in file_list:
            paths.append({'path': '/'.join(filename.split('/')[3:]), 'url': 'https://ml.crowdworks.kr:8080' + filename[1:]})

        result_json.append({
                    'label':label,
                    'paths': paths
                    })
                
    conn = getconn()
    cursor = conn.cursor()
    # 파일 리스트 JSON 가져 오기
    query = "SELECT ocr_files, bbox_files, seg_files, face_files, pose_files FROM task_summary WHERE transaction_id = {};".format(transaction_id)
    cursor.execute(query)
    result = cursor.fetchall()
    conn.commit()
    conn.close()

    ocr_files, bbox_files, seg_files, face_files, pose_files = result[0]

    for obj_class in selected_types:
        if obj_class in list(json.loads(ocr_files).keys()):
            result_json.append({
                            'label':obj_class,
                            'paths':json.loads(ocr_files)[obj_class]
                            })
            continue
        if obj_class in list(json.loads(bbox_files).keys()):
            result_json.append({
                            'label':obj_class,
                            'paths':json.loads(bbox_files)[obj_class]
                            })
            continue
        if obj_class in list(json.loads(seg_files).keys()):
            result_json.append({
                            'label':obj_class,
                            'paths':json.loads(seg_files)[obj_class]
                            })
            continue
        if obj_class in list(json.loads(face_files).keys()):
            result_json.append({
                            'label':obj_class,
                            'paths':json.loads(face_files)[obj_class]
                            })
            continue
        if obj_class in list(json.loads(pose_files).keys()):
            result_json.append({
                            'label':obj_class,
                            'paths':json.loads(pose_files)[obj_class]
                            })                                                                                 
            continue

    return jsonify(result_json)


@app.route('/cwmla/content/<transaction_id>/get_obj', methods=['GET'])
def get_object(transaction_id):
    res_list = []
    conn = getconn()
    cursor = conn.cursor()
    # 기본 정보 가져 오기
    query = "SELECT ocr_ready, ocr_summary, bbox_ready, bbox_summary, seg_ready, seg_summary, face_ready, face_summary, pose_ready, pose_summary FROM task_summary WHERE transaction_id = {};".format(transaction_id)
    cursor.execute(query)
    result = cursor.fetchall()
    conn.commit()
    conn.close()
    
    ocr_ready, ocr_summary, bbox_ready, bbox_summary, seg_ready, seg_summary, face_ready, face_summary, pose_ready, pose_summary = result[0]

    if ocr_ready == 1:
        for obj in json.loads(ocr_summary):
            res_list.append(obj['type'])
    if bbox_ready == 1:
        for obj in json.loads(bbox_summary):
            res_list.append(obj['type'])
    if seg_ready == 1:
        for obj in json.loads(seg_summary):
            res_list.append(obj['type'])
    if face_ready == 1:
        for obj in json.loads(face_summary):
            res_list.append(obj['type'])
    if pose_ready == 1:
        for obj in json.loads(pose_summary):
            res_list.append(obj['type'])
    
    return jsonify({"types": res_list})




@app.route('/cwmla/content/<transaction_id>/get_type_thumbnail', methods=['POST'])
def get_object_thumbnails(transaction_id):
    query = "SELECT ocr_files, bbox_files, seg_files, face_files, pose_files FROM task_summary WHERE transaction_id = {};".format(transaction_id)
    result = db_execute(query)
    ocr_files, bbox_files, seg_files, face_files, pose_files = result[0]
    
    request_data = request.get_json()
    engine = request_data['engine']
    types = request_data['types']

    thumbnail_list = []
    if engine == 'ocr':
        for req_type in types:
            for item in json.loads(ocr_files)[req_type]:
                url = item['url'].replace('https://ml.crowdworks.kr:8080/data/', 'https://ml.crowdworks.kr:8080/thumbnail/')
                path = "/".join(item['path'].split("/")[3:])
                thumbnail_list.append({'url': url, 'path': path})
    elif engine == 'bbox':
        for req_type in types:
            for item in json.loads(bbox_files)[req_type]:
                url = item['url'].replace('https://ml.crowdworks.kr:8080/data/', 'https://ml.crowdworks.kr:8080/thumbnail/')
                path = "/".join(item['path'].split("/")[3:])
                thumbnail_list.append({'url': url, 'path': path})
    elif engine == 'seg':
        for req_type in types:
            for item in json.loads(seg_files)[req_type]:
                url = item['url'].replace('https://ml.crowdworks.kr:8080/data/', 'https://ml.crowdworks.kr:8080/thumbnail/')
                path = "/".join(item['path'].split("/")[3:])
                thumbnail_list.append({'url': url, 'path': path})
    elif engine == 'facelandmark':
        for req_type in types:
            for item in json.loads(face_files)[req_type]:
                url = item['url'].replace('https://ml.crowdworks.kr:8080/data/', 'https://ml.crowdworks.kr:8080/thumbnail/')
                path = "/".join(item['path'].split("/")[3:])
                thumbnail_list.append({'url': url, 'path': path})
    elif engine == 'pose':
        for req_type in types:
            for item in json.loads(pose_files)[req_type]:
                url = item['url'].replace('https://ml.crowdworks.kr:8080/data/', 'https://ml.crowdworks.kr:8080/thumbnail/')
                path = "/".join(item['path'].split("/")[3:])
                thumbnail_list.append({'url': url, 'path': path})
    else:
        thumbnail_list.append({'url': [], 'path': []})

    print(thumbnail_list)
    
    return jsonify({"thumbnail_list": thumbnail_list})

@app.route('/data/<path:path>')
def send_data_folder(path):
    import urllib.parse
    return send_from_directory('./data', path)
    # 파일명에 공백이 있는 경우 %20으로 인코딩 되서 날아오는 데 못찾는다.. _ 로 공백 변경 필요 (압축 해제 후)


@app.route('/thumbnail/<path:path>')
def send_thumbnail_folder(path):
    import urllib.parse
    return send_from_directory('./thumbnail', path)
    # 파일명에 공백이 있는 경우 %20으로 인코딩 되서 날아오는 데 못찾는다.. _ 로 공백 변경 필요 (압축 해제 후)

@app.route('/purge')
def app_purge():
    celery.control.purge()
    return 0

if __name__ == '__main__':
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS)
    ssl_context.load_cert_chain(certfile='./cert/ml.crowdworks.kr/star_crowdworks_kr_cert.crt', keyfile='./cert/ml.crowdworks.kr/key_nopass.pem')
    app.run(host='0.0.0.0', port=8080, ssl_context=ssl_context)


